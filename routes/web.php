<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/set', 'GoogleSheetsController@setRange');
$router->get('/fields', 'BitrixController@getFieldsList');

$router->group(['prefix' => 'integrate'], function () use ($router) {
    $router->get('integration', 'GoogleSheetsController@integration');
    $router->get('review', 'GoogleSheetsController@financialReview');
    $router->get('graphic', 'GoogleSheetsController@setGraphicOne');
    $router->get('file', 'GoogleSheetsController@downloadFile');
    $router->get('review_t', 'GoogleSheetsController@getTechnicalReview');
    $router->get('copy', 'TechnicalVisitController@createCopy');
    $router->get('list', 'TasksController@countDate');
    $router->get('finish', 'TechnicalVisitController@finish');
    $router->post('integration', 'GoogleSheetsController@integration');
    $router->post('review', 'GoogleSheetsController@financialReview');
    $router->post('file', 'GoogleSheetsController@downloadFile');
    $router->post('copy', 'TechnicalVisitController@createCopy');
    $router->post('list', 'TasksController@countDate');
    $router->post('finish', 'TechnicalVisitController@finish');
});



