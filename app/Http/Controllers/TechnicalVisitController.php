<?php

namespace App\Http\Controllers;

use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use App\GoogleSheets;
use App\Http\Controllers\BitrixController;
use App\Bitrix\Bitrix24Webhook;
use Exception;
use Illuminate\Http\Request;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_Permission;

class TechnicalVisitController extends Controller
{
    public function __construct()
    {
        $webhook = "https://enerzee.bitrix24.com.br/rest/389/9ahduwm3u4g2lboe/";
        $this->bx24 = new Bitrix24Webhook($webhook);
        $this->spreadsheetId = '1hPHq0rGIA7hDq2YiZ2pNoMOxVNFsYg1X05jfj8tBkuk';
    }

    public function createCopy(Request $request){
        //pega id do negocio da url
        $deal_id = $request->input('deal_id');
        //Duplica planilha base
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Drive($client);
        $postBody = new Google_Service_Drive_DriveFile();
        $return = $service->files->copy('1hPHq0rGIA7hDq2YiZ2pNoMOxVNFsYg1X05jfj8tBkuk', $postBody);
        //pega o id da nova planilha
        $file_id = $return->id;

        $serviceSheet = new Google_Service_Sheets($client);
        $range = "G276";
        $body = new Google_Service_Sheets_ValueRange ([
            'values' => [["=HIPERLIGAÇÃO(\"https://gerador.enerzee.com.br/integrate/finish?file_id=$file_id&deal_id=$deal_id\";\"Clique no link para concluir\")"]]
        ]);
        
        $params = [  
            'valueInputOption' => "USER_ENTERED" 
        ];
        $result = $serviceSheet->spreadsheets_values->update( $file_id, $range, $body, $params );

        $permission = new Google_Service_Drive_Permission();
        $permission->setType('anyone');
        $permission->setRole('writer');
        $response = $service->permissions->create($file_id, $permission);
    
        $update = $this->updateLinkDeal($deal_id, $file_id);
        // TODO: gerar link para chamar a função que irá baixar o arquivo no negocio
        // salvar esse link na planilha que acabou de ser gerada, contendo o id do negocio e o id do arquivo
        // esse link deverá levar ao metodo abaixo
        //retorna resultado, apenas visual
        return response()->json([
            "id" => $file_id,
            "response" => $return
        ]);
    }

    public function updateLinkDeal($deal_id, $file_id){
        $params = [
            "ID" => $deal_id,
            "fields" => [
                "UF_CRM_1595954215" => "https://docs.google.com/spreadsheets/d/" . $file_id . "/edit?usp=sharing"
            ]
        ];

        $update = $this->bx24->callMethod("crm.deal.update", $params)->result;

        return $update;        
    }

    public function updateFileDeal($id_drive_file, $deal_id){
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Drive($client);

        $response = $service->files->export(
            $id_drive_file,
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            array('alt' => 'media')
        );
        $content = $response->getBody()->getContents();
        $content_base64 = base64_encode($content);

        $result = $this->bx24->callMethod('crm.deal.update', [
            "ID" => $deal_id,
            "FIELDS" => [
                "UF_CRM_1595954132" => ["fileData"=>["checklist_de_visita_tecnica.xlsx", $content_base64]]
            ]
        ]);

        $this->moveStage($deal_id);

        return true;
    }

    public function finish(Request $request){
        $id_drive_file = $request->input('file_id');
        $deal_id = $request->input('deal_id');   
        
        $file = $this->updateFileDeal($id_drive_file, $deal_id);

        //TODO: baixar arquivo com id do drive e jogar em um campo do negocio passado
        return "Envio concluído com sucesso!";

    }

    public function moveStage($deal_id){
        $stage = "C8:13";

        $params = [
            "ID" => $deal_id,
            "FIELDS" => [
                "STAGE_ID" => $stage
            ]
        ];

        $response = $this->bx24->callMethod("crm.deal.update", $params)->result;

        return $response;
    }

}