<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 1200);

use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Google_Service_Sheets_BatchUpdateValuesRequest;
use App\GoogleSheets;
use App\Config\Mapping;
use App\Http\Controllers\BitrixController;
use App\Bitrix\Bitrix24Webhook;
use Exception;
use Illuminate\Http\Request;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_DriveFileContentHintsThumbnail;
use Google_Service_Drive_FileList;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class GoogleSheetsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $webhook = "https://enerzee.bitrix24.com.br/rest/389/9ahduwm3u4g2lboe/";
        $this->bx24 = new Bitrix24Webhook($webhook);
        $this->spreadsheetId = '1VaLZZAurCh7F0m87C1NQPyWcCk6nvEAxuZLGWWk0JE0';
        $this->typeOfSystem = [
            "#N/A" => 179,
            "Microgeração" => 179,
            "Minigeração" => 181
        ];
        $this->modalidadeVenda = [
            7327 => "Venda Direta - Enerzee",
            7329 => "Convênio - Cocari"
        ];
        $this->caboAC = [
            "Sim" => 7479,
            "Não" => 7481
        ];
        $this->servicoeMaterial = [
            "Sim" => 7483,
            "Não" => 7485
        ];
        $this->infraestrutura = [
            "Sim" => 7487,
            "Não" => 7489
        ];
        $this->extensaoRede = [
            "Sim" => 7491,
            "Não" => 7493
        ];
        $this->cabineMedicao = [
            "Sim" => 7495,
            "Não" => 7497
        ];
        $this->subestacao = [
            "Sim" => 7499,
            "Não" => 7501
        ];
        $this->transformadorAcoplamento = [
            "Sim" => 7503,
            "Não" => 7505
        ];
        $this->padraoEntrada = [
            "Sim" => 7507,
            "Não" => 7509
        ];

    }

    public function integration(Request $request){
        $id = $request->input('deal_id');
        
        $deal = $this->getDeal($id);

         if($deal->UF_CRM_1597355806 == "Sim"){
             echo "Entrou revisão técnica";
             $bitrixToSheet = $this->bitrixToSheet($id);
             $review = $this->getTechnicalReview($id);
             $sheetToBitrix = $this->sheetToBitrix($id);
             $file = $this->downloadFileReview($id);
             $reviewNo = $this->setToNoReview();
        }
        else if($deal->UF_CRM_1597065495 == "Sim"){
            echo "Entrou revisão financeira";
            $sheetToBitrix = $this->sheetToBitrix($id);
            $file = $this->downloadFileReview($id);
            $reviewNo = $this->setToNoFinancialReview();
            $move = $this->moveStageReview($id);
        }
        else{
            echo "Não entrou revisão";
            $bitrixToSheet = $this->bitrixToSheet($id);
            $sheetToBitrix = $this->sheetToBitrix($id);
        }
        

        return "";
    }

    public function sheetToBitrix($id){
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Sheets($client);

        $deal = $this->getDeal($id);

        $mapSheet = Mapping::getSheetsToBitrix();

        $ranges = [];
        foreach($mapSheet as $m){
            $ranges[] = "Saída!".$m[0];
        }
        $params = array(
            'ranges' => $ranges
        );
        $response = $service->spreadsheets_values->batchGet($this->spreadsheetId, $params);
        $values = $response->getValueRanges();

        $bitrix_fields_to_update = [];
        $values_assoc_array = [];
        foreach($values as $value){
            if(isset($value->values)){
                $range = str_replace("'Saída'!", "",$value->range);
                $values_assoc_array[$range] = $value->values[0][0];
            }
        }
        $values_assoc_array = $this->formatMoney($values_assoc_array);   
        //dd($values_assoc_array);    
        foreach($mapSheet as $map){
            if(isset($values_assoc_array[$map[0]])){
                $bitrix_fields_to_update[$map[1]] = $values_assoc_array[$map[0]];
            }
        }
        //dd($bitrix_fields_to_update);
        $bitrix_fields_to_update["UF_CRM_1560428277150"] = $this->typeOfSystem[$bitrix_fields_to_update["UF_CRM_1560428277150"]];

        $bitrix_fields_to_update["UF_CRM_1598532638"] = $this->caboAC[$bitrix_fields_to_update["UF_CRM_1598532638"]];
        $bitrix_fields_to_update["UF_CRM_1598532680"] = $this->servicoeMaterial[$bitrix_fields_to_update["UF_CRM_1598532680"]];
        $bitrix_fields_to_update["UF_CRM_1598532717"] = $this->infraestrutura[$bitrix_fields_to_update["UF_CRM_1598532717"]];
        $bitrix_fields_to_update["UF_CRM_1598532761"] = $this->extensaoRede[$bitrix_fields_to_update["UF_CRM_1598532761"]];
        $bitrix_fields_to_update["UF_CRM_1598532842"] = $this->cabineMedicao[$bitrix_fields_to_update["UF_CRM_1598532842"]];
        $bitrix_fields_to_update["UF_CRM_1598532997"] = $this->subestacao[$bitrix_fields_to_update["UF_CRM_1598532997"]];
        $bitrix_fields_to_update["UF_CRM_1598533035"] = $this->transformadorAcoplamento[$bitrix_fields_to_update["UF_CRM_1598533035"]];
        $bitrix_fields_to_update["UF_CRM_1598533074"] = $this->padraoEntrada[$bitrix_fields_to_update["UF_CRM_1598533074"]];
        foreach($bitrix_fields_to_update as $field=>$value){
            if($value == "#VALUE!" || $value == "0" || $value == " " || $value == "#N/A"){
                $bitrix_fields_to_update[$field] = "";
            }
        }
        //dd($bitrix_fields_to_update);
        
        $result = $this->bx24->callMethod('crm.deal.update', [
            'ID' => $id,
            'FIELDS' => $bitrix_fields_to_update
        ])->result;

        $graphic_1 = $this->setGraphicOne($id);
        $graphic_2 = $this->setGraphicTwo($id);
        $file = $this->downloadFile($id);

        $moveStage = $this->moveStage($id, $bitrix_fields_to_update["UF_CRM_1560428277150"], $deal->UF_CRM_1595853615, $deal);

        return response()->json([
            'result_bitrix' => $result,
            'values_sheets' => $values_assoc_array 
        ]);
    }
    public function bitrixToSheet($id){
        try{
            $elements = $this->getList($id);

            $deal = $this->getDeal($id);

            
            $map = Mapping::getBitrixToSheetsList();
            $mapDeal = Mapping::getBitrixToSheetsDeal();

            
            $mappedDeal = $this->mapBitrixDealToKeyValue($deal, $mapDeal);
            $mappedList = $this->mapBitrixListToKeyValue($elements, $map);
            $mappedList = $this->formatData($mappedList);
            //dd($mappedList);

            $responsabilidade_cabine = "UF_CRM_1598384718";
            $cabine_cliente = 7431;
            $responsabilidade_subestacao = "UF_CRM_1598384792";
            $subestacao_cliente = 7435;
            $responsabilidade_transformador = "UF_CRM_1598384922";
            $transformador_cliente = 7439;
            $responsabilidade_padrao_entrada = "UF_CRM_1598385013";
            $padrao_entrada_cliente = 7443;
            $responsabilidade_infra = "UF_CRM_1598385178";
            $infra_cliente = 7447;
            $responsabilidade_rede = "UF_CRM_1598385314";
            $rede_cliente = 7451;

            if($deal->$responsabilidade_cabine == $cabine_cliente){
                $mappedDeal["cabine_de_medicao"][1] = "";
                $mappedDeal["cabine_de_medicao_qtd"][1] = "";
            }

            if($deal->$responsabilidade_subestacao == $subestacao_cliente){
                $mappedDeal["subestacao_um"][1] = "";
                $mappedDeal["subestacao_um_qtd"][1] = "";
                $mappedDeal["subestacao_dois"][1] = "";
                $mappedDeal["subestacao_dois_qtd"][1] = "";
            }

            if($deal->$responsabilidade_transformador == $transformador_cliente){
                $mappedDeal["transformador_de_acoplamento_um"][1] = "";
                $mappedDeal["transformador_de_acoplamento_um_qtd"][1] = "";
                $mappedDeal["transformador_de_acoplamento_dois"][1] = "";
                $mappedDeal["transformador_de_acoplamento_dois_qtd"][1] = "";
            }
            if($deal->$responsabilidade_padrao_entrada == $padrao_entrada_cliente){
                $mappedDeal["padrao_de_entrada"][1] = "";
                $mappedDeal["padrao_de_entrada_qtd"][1] = "";
            }
            if($deal->$responsabilidade_infra == $infra_cliente){
                $mappedDeal["infraestrutura_e_serviços_civil_reais"][1] = "";
                $mappedDeal["infraestrutura_e_serviços_civil_qtd"][1] = "";
            }
            if($deal->$responsabilidade_rede == $rede_cliente){
                $mappedDeal["extensao_de_rede_reais"][1] = "";
                $mappedDeal["extensao_de_rede_qtd"][1] = "";
            }

            $mappedDeal["modalidade_de_venda"][1] = $this->modalidadeVenda[$mappedDeal["modalidade_de_venda"][1]];

            $mapped = array_merge( $mappedDeal, $mappedList);    

            //dd($mapped);
            
            $ranges = [];
            $values = [];
            foreach($mapped as $m){
                $ranges[] = $m[0];
                $values[] = $m[1];
            }

            $client = GoogleSheets::getClient();
            $service = new Google_Service_Sheets($client);
            
            $data = [];
            foreach($values as $i => $v){
                    $data[] = new Google_Service_Sheets_ValueRange([
                        'range' => $ranges[$i],
                        'values' => [[$values[$i]]]
                    ]);
            }

            // Additional ranges to update ...
            $body = new Google_Service_Sheets_BatchUpdateValuesRequest([
                'valueInputOption' => "USER_ENTERED",
                'data' => $data
            ]);
            
            $result = $service->spreadsheets_values->batchUpdate($this->spreadsheetId, $body);
            response()->json($result);
        }catch(\Exception $ex){
            dd($ex);
            dd($ex->getMessage());
        }
    }
    public function mapBitrixListToKeyValue($elements, $map){
        $aux_element = [];
        
        for($i = count($elements) - 1; $i < 10 ; $i++){
            $elements[] = new \stdClass();
        }
        $sum = 0;
        foreach($elements as $i => $element){    
            foreach($map as $field_name => $m){
                if(isset($m[1][$i])){
                    if(isset($element->{$m[0]})){
                        $aux_element[$field_name . '_' . $i] = [ $m[1][$i], current((Array)$element->{$m[0]})];
                    }else{
                        $aux_element[$field_name . '_' . $i] = [ $m[1][$i], ""];
                    }
                }
            }
            if(isset($element->PROPERTY_174)){
                if(current((Array)$element->PROPERTY_174) == "214"){
                    if(isset($aux_element["UC_A_".$i])){
                        $aux_element["UC_A_".$i] = [$aux_element["UC_A_".$i][0], ""];
                    }
                    if(isset( $aux_element["concess_A_".$i])){
                        $aux_element["concess_A_".$i] = [$aux_element["concess_A_".$i][0], ""];
                    }      
                }else{
                    $aux_element["UC_B_".$i] = [$aux_element["UC_B_".$i][0], ""];
                    $aux_element["concess_B_".$i] = [$aux_element["concess_B_".$i][0], ""];
                }
            }else{
                // unset($aux_element["UC_B_".$i]);
                // unset($aux_element["concess_B_".$i]);
                // unset($aux_element["UC_A_".$i]);
                // unset($aux_element["concess_A_".$i]);
                unset($aux_element["potencia_do_modulo_".$i]);
                unset($aux_element["telhado_".$i]);
                unset($aux_element["tensao_".$i]);
                unset($aux_element["media_".$i]);
                unset($aux_element["tarifa_com_tributos_".$i]);
                // unset($aux_element["consumo_desejado_".$i]);
                unset($aux_element["tarifa_demanda_fora_ponta_".$i]);
                unset($aux_element["tarifa_fora_ponta_".$i]);
                unset($aux_element["tarifa_ponta_".$i]);
                unset($aux_element["demanda_contratada_".$i]);
                unset($aux_element["consumo_medio_fora_ponta_".$i]);
                unset($aux_element["consumo_medio_ponta_".$i]);
            }

            if(isset($aux_element["media_".$i])){
                $aux_element["media_".$i][1] = str_replace(',', '.', $aux_element["media_".$i][1]);
                $sum += (float)$aux_element["media_".$i][1];
            }
            
            if(isset($aux_element["media_".$i])){
                $aux_element["media_".$i][1] = $sum;
                $aux_element["media_".$i][1] = str_replace('.', ',', $aux_element["media_".$i][1]);
            } 

            if(isset($aux_element["media_".$i])){
                if($aux_element["media_".$i][1] == "0"){
                    $aux_element["media_".$i][1] = "";
                }            
            }          
        }
        //dd($aux_element);
        return $aux_element;
    }
    public function mapBitrixDealToKeyValue($deal, $map){
        $aux_element = [];
        foreach($map as $field_name => $m){
            if(isset($deal->{$m[0]})){
                $aux_element[$field_name] = [ $m[1], $deal->{$m[0]}];
            }else{
                $aux_element[$field_name] = [ $m[1], ""];
            }
        }
        return $aux_element;
    }

    private function getList($id){
        $params = [
            "IBLOCK_TYPE_ID" => "bitrix_processes",
            "IBLOCK_ID" => 40,
            "filter" => [
                "PROPERTY_138" => $id
            ]
        ];

        $elements = $this->bx24->callMethod("lists.element.get", $params)->result;

        return $elements;
    }
    private function getDeal($id){
        $deal = $this->bx24->callMethod("crm.deal.get", ["ID"=>$id])->result;

        return $deal;
    }
    private function formatData($mappedList){
        for($i = 0; $i<10; $i++){
            if(isset($mappedList['tarifa_com_tributos' . '_' .$i][1])){
                $mappedList['tarifa_com_tributos' . '_' .$i][1] = str_replace("|BRL", "", $mappedList['tarifa_com_tributos' . '_' .$i][1]);
            }
            if(isset($mappedList['tarifa_demanda_fora_ponta' . '_' .$i][1])){
                $mappedList['tarifa_demanda_fora_ponta' . '_' .$i][1] = str_replace("|BRL", "", $mappedList['tarifa_demanda_fora_ponta' . '_' .$i][1]);
            }
            if(isset($mappedList['tarifa_fora_ponta' . '_' .$i][1])){
                $mappedList['tarifa_fora_ponta' . '_' .$i][1] = str_replace("|BRL", "", $mappedList['tarifa_fora_ponta' . '_' .$i][1]);
            }
            if(isset($mappedList['tarifa_ponta' . '_' .$i][1])){
                $mappedList['tarifa_ponta' . '_' .$i][1] = str_replace("|BRL", "", $mappedList['tarifa_ponta' . '_' .$i][1]);
            } 
        }
            return $mappedList;
    }

    private function formatMoney($values){

        if(isset($values['E14'])){
            $values['E14'] = $this->formatedMoneyToNumber($values['E14']);
        }
        if(isset($values['C20'])){
            $values['C20'] = $this->formatedMoneyToNumber($values['C20']);
        }
        if(isset($values['C23'])){
            $values['C23'] = $this->formatedMoneyToNumber($values['C23']);
        }
        if(isset($values['E20'])){
            $values['E20'] = $this->formatedMoneyToNumber($values['E20']);
        }
        if(isset($values['E23'])){
            $values['E23'] = $this->formatedMoneyToNumber($values['E23']);
        }
        if(isset($values['E26'])){
            $values['E26'] = $this->formatedMoneyToNumber($values['E26']);
        }
        if(isset($values['E69'])){
            $values['E69'] = $this->formatedMoneyToNumber($values['E69']);
        }
        if(isset($values['E72'])){
            $values['E72'] = $this->formatedMoneyToNumber($values['E72']);
        } 
        if(isset($values['E75'])){
            $values['E75'] = $this->formatedMoneyToNumber($values['E75']);
        }  
        if(isset($values['C78'])){
            $values['C78'] = $this->formatedMoneyToNumber($values['C78']);
        }  
        if(isset($values['C81'])){
            $values['C81'] = $this->formatedMoneyToNumber($values['C81']);
        }  
        if(isset($values['C142'])){
            $values['C142'] = $this->formatedMoneyToNumber($values['C142']);
        }  
        if(isset($values['C143'])){
            $values['C143'] = $this->formatedMoneyToNumber($values['C143']);
        }  
        if(isset($values['C144'])){
            $values['C144'] = $this->formatedMoneyToNumber($values['C144']);
        } 
        if(isset($values['C145'])){
            $values['C145'] = $this->formatedMoneyToNumber($values['C145']);
        } 
        if(isset($values['C146'])){
            $values['C146'] = $this->formatedMoneyToNumber($values['C146']);
        } 
        if(isset($values['E142'])){
            $values['E142'] = $this->formatedMoneyToNumber($values['E142']);
        }  
        if(isset($values['E143'])){
            $values['E143'] = $this->formatedMoneyToNumber($values['E143']);
        }  
        if(isset($values['E144'])){
            $values['E144'] = $this->formatedMoneyToNumber($values['E144']);
        }  
        if(isset($values['C149'])){
            $values['C149'] = $this->formatedMoneyToNumber($values['C149']);
        }  
        if(isset($values['C150'])){
            $values['C150'] = $this->formatedMoneyToNumber($values['C150']);
        }                

        return $values;
    }

    private function formatedMoneyToNumber($v){
        $v = str_replace("R$ ", "", $v);
        $v = str_replace(".", "", $v);
        $v = str_replace(",", ".", $v);
        return $v;
        
    }

    private function getResponsible($deal){
        $responsible = $this->bx24->callMethod("crm.contact.get", ["ID" => $deal->UF_CRM_1592855066])->result;

        return $responsible;
    }

    private function moveStage($id, $tipo, $condicao, $deal){
        $sim = 7363;
        $nao = 7365;
        $micro = 179;
        $mini = 181;
        $elaboracaoProposta = "C8:PREPARATION";
        $propostaEnviada = "C8:FINAL_INVOICE";
        $aprovacaoDiretoria = "C8:8";
        $estagio = "";

        $responsible = $this->getResponsible($deal);

        if($tipo == $micro){
            if($condicao == $sim){
                $estagio = $propostaEnviada;
            }
            else if ($condicao == $nao){
                if($responsible->UF_CRM_1594649998141 == "Sim"){
                    $estagio = $propostaEnviada;
                }
                else if($responsible->UF_CRM_1594649998141 == "Não" || $responsible->UF_CRM_1594649998141 == ""){
                    $estagio = $elaboracaoProposta;
                }
            }
        }else if ($tipo == $mini){
            if($condicao == $sim){
                $estagio = $aprovacaoDiretoria;
            }
            else if($condicao == $nao){
                $estagio = $elaboracaoProposta;  
            }
        }
        $result = $this->bx24->callMethod('crm.deal.update', [
            'ID' => $id,
            'FIELDS' => [
                "STAGE_ID" => $estagio
            ]
        ])->result;

        return $result;
    }

    public function downloadFile($id){
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Drive($client);

        $response = $service->files->export(
            $this->spreadsheetId,
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            array('alt' => 'media')
        );
        $content = $response->getBody()->getContents();
        $content_base64 = base64_encode($content);

        $result = $this->bx24->callMethod('crm.deal.update', [
            "ID" => $id,
            "FIELDS" => [
                "UF_CRM_1596039781" => ["fileData"=>["proposta.xlsx", $content_base64]]
            ]
        ]);
        
        return true;
            
    }
    public function downloadFileReview($id){
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Drive($client);

        $response = $service->files->export(
            $this->spreadsheetId,
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            array('alt' => 'media')
        );
        $content = $response->getBody()->getContents();
        $content_base64 = base64_encode($content);

        $result = $this->bx24->callMethod('crm.deal.update', [
            "ID" => $id,
            "FIELDS" => [
                "UF_CRM_1596039781" => ["fileData"=>["revisao.xlsx", $content_base64]]
            ]
        ]);
        
        return true;
            
    }


    // Gráficos
    public function setGraphicOne($id){
        
        $field_graphic_1 = "UF_CRM_1598895599";
        
        $graph = Mapping::getGraphOne();
        $graphOne = [];
        foreach($graph as $field_name => $g){
            $graphOne[$field_name] = [$g[0]];
        }
    
        foreach($graphOne as $g){
            $ranges[] = "Saída!".$g[0];
        }

        $client = GoogleSheets::getClient();
        $service = new Google_Service_Sheets($client);

        $params = array(
            'ranges' => $ranges
        );
        $response = $service->spreadsheets_values->batchGet($this->spreadsheetId, $params);
        $values = $response->getValueRanges();
        
        $values_graph = [];
        foreach($values as $value){
            $values = str_replace("R$", "",$value->values[0][0]);
            $values = trim($values);
            $values = str_replace(".", "", $values);
            $values_graph[] = $values;
        }


        $chart_data = [
            "type" => "bar", // Show a bar chart
            "data" => [
              "labels" => ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],   // Set X-axis labels
              "datasets" => [[
                "label" => 'Antes(R$)',                         // Create the 'Users' dataset
                "data" => [...array_slice($values_graph,0,12)],           // Add data to the chart
                "borderColor" => 'blue',
                "backgroundColor" => "blue"
              ],[
                "label" => 'Depois(R$)',                         // Create the 'Users' dataset
                "data" => [...array_slice($values_graph,12,23)],           // Add data to the chart
                "borderColor" => 'orange',
                "backgroundColor" => "orange"
              ]]
            ]
        ];

        $image_data = "https://quickchart.io/chart?bkg=white&c=" . json_encode($chart_data);

        try{
            $imageData = base64_encode(file_get_contents($image_data));
            //echo $image_data;
            $params = [
                "ID" => $id,
                "fields" => [
                    $field_graphic_1 => ["fileData" => ["grafico_instalacao.jpg", $imageData]]
                ]
            ];

            $update = $this->bx24->callMethod("crm.deal.update", $params)->result;

            return $update;      
        }catch(Exception $e){
            return " ";
        }      
    }
    public function setGraphicTwo($id){
        $field_graphic_2 = "UF_CRM_1598975854";
        
        $graph = Mapping::getGraphTwo();
        $graphTwo = [];
        foreach($graph as $field_name => $g){
            $graphTwo[$field_name] = [$g[0]];
        }
    
        foreach($graphTwo as $g){
            $ranges[] = "Saída!".$g[0];
        }

        $client = GoogleSheets::getClient();
        $service = new Google_Service_Sheets($client);

        $params = array(
            'ranges' => $ranges
        );
        $response = $service->spreadsheets_values->batchGet($this->spreadsheetId, $params);
        $values = $response->getValueRanges();
        
        $values_graph = [];
        foreach($values as $value){
            $values = str_replace("R$", "",$value->values[0][0]);
            $values = trim($values);
            $values = str_replace(" ", "",$values);
            $values = str_replace(".", "", $values);
            $values_graph[] = $values;
        }

        $chart_data = [
            "type" => "line", // Show a bar chart
            "data" => [
              "labels" => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],   // Set X-axis labels
              "datasets" => [[
                "label" => 'Payback',                         // Create the 'Users' dataset
                "data" => [...$values_graph],           // Add data to the chart
                "fill" => false,
                "borderColor" => 'blue'
                ]]
            ]
        ];

        $image_data = "https://quickchart.io/chart?bkg=white&c=" . json_encode($chart_data);

        try{
            $imageData = base64_encode(file_get_contents($image_data));
            $params = [
                "ID" => $id,
                "fields" => [
                    $field_graphic_2 => ["fileData" => ["grafico_instalacao.jpg", $imageData]]
                ]
            ];

            $update = $this->bx24->callMethod("crm.deal.update", $params)->result;

            return $update;
        }catch(Exception $e){
            return " ";
        }    
    }

    //Revisão Técnica
    public function getTechnicalReview($id){
        try{

            $responsabilidade_cabine = "UF_CRM_1598384718";
            $cabine_cliente = 7431;
            $responsabilidade_subestacao = "UF_CRM_1598384792";
            $subestacao_cliente = 7435;
            $responsabilidade_transformador = "UF_CRM_1598384922";
            $transformador_cliente = 7439;
            $responsabilidade_padrao_entrada = "UF_CRM_1598385013";
            $padrao_entrada_cliente = 7443;
            $responsabilidade_infra = "UF_CRM_1598385178";
            $infra_cliente = 7447;
            $responsabilidade_rede = "UF_CRM_1598385314";
            $rede_cliente = 7451;

            $elements = $this->getListReview($id);

            $deal = $this->getDeal($id);

            $mapList = Mapping::getReviewList();
            $mapDeal = Mapping::getReviewDeal();
            
            $mappedDeal = $this->mapBitrixDealToKeyValue($deal, $mapDeal);
            $mappedList = $this->mapListReviewToKeyValue($elements, $mapList);
            
            if($deal->$responsabilidade_cabine == $cabine_cliente){
                $mappedList["cabine_de_medição"][1] = "";
                $mappedList["cabine_de_medição_reais"][1] = "";
                $mappedList["cabine_de_medição_qtd"][1] = "";
            }

            if($deal->$responsabilidade_subestacao == $subestacao_cliente){
                $mappedList["subestacao_um"][1] = "";
                $mappedList["subestacao_um_qtd"][1] = "";
                $mappedList["subestacao_dois"][1] = "";
                $mappedList["subestacao_dois_qtd"][1] = "";
                $mappedList["subestacao_total"][1] = "";
            }
            if($deal->$responsabilidade_transformador == $transformador_cliente){
                $mappedList["transformador_de_acoplamento_um"][1] = "";
                $mappedList["transformador_de_acoplamento_um_qtd"][1] = "";
                $mappedList["transformador_de_acoplamento_dois"][1] = "";
                $mappedList["transformador_de_acoplamento_dois_qtd"][1] = "";
                $mappedList["transformador_de_acoplamento_total"][1] = "";
            }
            if($deal->$responsabilidade_padrao_entrada == $padrao_entrada_cliente){
                $mappedList["padrao_de_entrada"][1] = "";
                $mappedList["padrao_de_entrada_reais"][1] = "";
                $mappedList["padrao_de_entrada_qtd"][1] = "";
            }
            if($deal->$responsabilidade_infra == $infra_cliente){
                $mappedList["infraestrutura_e_serviços_civil"][1] = "";
                $mappedList["infraestrutura_e_serviços_civil_reais"][1] = "";
                $mappedList["infraestrutura_e_serviços_civil_qtd"][1] = "";
            }
            if($deal->$responsabilidade_rede == $rede_cliente){
                $mappedList["extensao_de_rede"][1] = "";
                $mappedList["extensao_de_rede_reais"][1] = "";
                $mappedList["extensao_de_rede_qtd"][1] = "";
            }

            $mapped = array_merge( $mappedDeal, $mappedList);

            $ranges = [];
            $values = [];
            foreach($mapped as $m){
                $ranges[] = "Revisão - Técnica!".$m[0];
                $values[] = $m[1];
            }

            $client = GoogleSheets::getClient();
            $service = new Google_Service_Sheets($client);
            
            $data = [];
            foreach($values as $i => $v){
                $data[] = new Google_Service_Sheets_ValueRange([
                    'range' => $ranges[$i],
                    'values' => [[$values[$i]]]
                ]);
            }
            // Additional ranges to update ...
            $body = new Google_Service_Sheets_BatchUpdateValuesRequest([
                'valueInputOption' => "USER_ENTERED",
                'data' => $data
            ]);
            
            $result = $service->spreadsheets_values->batchUpdate($this->spreadsheetId, $body);
            response()->json($result);
        }catch(\Exception $ex){
            dd($ex);
            dd($ex->getMessage());
        }
    }
    public function mapListReviewToKeyValue($element, $map){
        $aux_element = [];
        foreach($element as $i => $element){    
            foreach($map as $field_name => $m){
                if(isset($m[1][$i])){
                    if(isset($element->{$m[0]})){
                        $aux_element[$field_name] = [ $m[1], current((Array)$element->{$m[0]})];
                    }else{
                        $aux_element[$field_name] = [ $m[1], ""];
                    }
                }
            }
        }
        //dd($aux_element);
        return $aux_element;
    }
    public function getListReview($id){
        
        $params = [
            "IBLOCK_TYPE_ID" => "bitrix_processes",
            "IBLOCK_ID" => 51,
            "filter" => [
                "PROPERTY_303" => $id
            ]
        ];

        $elements = $this->bx24->callMethod("lists.element.get", $params)->result;
        return $elements;
    }
    public function setToNoReview(){
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Sheets($client);
    
        $range = "Revisão - Técnica!C6";
        $body = new Google_Service_Sheets_ValueRange([
            'values' => [["Não"]]
        ]);

        $params = [
            'valueInputOption' => "USER_ENTERED"
        ];
        
        $result = $service->spreadsheets_values->update($this->spreadsheetId, $range, $body, $params);
        printf("%d cells updated.", $result->getUpdatedCells());
    }


    //Revisão Financeira!!
    public function financialReview(Request $request){
        $id = $request->input('deal_id');
        $desconto_ufv = $request->input('desconto_ufv');
        $desconto_ca = $request->input('desconto_ca');
        $revisao = $request->input('revisao');

        $bitrixToSheet = $this->bitrixToSheet($id);
        $setReview = $this->setfinancialReview($desconto_ufv, $desconto_ca, $revisao);

        $mapReview = Mapping::getFinancialReview();
        $mappedReview = [];
        foreach($mapReview as $field_name => $m){
            $mappedReview[$field_name] = [$m[0]];
        }
    
        foreach($mappedReview as $m){
            $ranges[] = "Revisão - Financeiro!".$m[0];
        }

        $client = GoogleSheets::getClient();
        $service = new Google_Service_Sheets($client);

        $params = array(
            'ranges' => $ranges
        );
        $response = $service->spreadsheets_values->batchGet($this->spreadsheetId, $params); 
        $values = $response->getValueRanges();

        return array($values);
    }
    public function setfinancialReview($desconto_ufv, $desconto_ca, $revisao){
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Sheets($client);

        $data = [];
        $data[] = new Google_Service_Sheets_ValueRange([
            'range' => "Revisão - Financeiro!E13",
            'values' => [[$desconto_ufv]]
        ]);
        $data[] = new Google_Service_Sheets_ValueRange([
            'range' => "Revisão - Financeiro!E22",
            'values' => [[$desconto_ca]]
        ]);
        $data[] = new Google_Service_Sheets_ValueRange([
            'range' => "Revisão - Financeiro!C5",
            'values' => [[$revisao]]
        ]);
    
        // Additional ranges to update ...
        $body = new Google_Service_Sheets_BatchUpdateValuesRequest([
            'valueInputOption' => "USER_ENTERED",
            'data' => $data
        ]);
        
        $result = $service->spreadsheets_values->batchUpdate($this->spreadsheetId, $body);
        response()->json($result);
    }
    public function setToNoFinancialReview(){
        $client = GoogleSheets::getClient();
        $service = new Google_Service_Sheets($client);
    
        $range = "Revisão - Financeiro!C5";
        $body = new Google_Service_Sheets_ValueRange([
            'values' => [["Não"]]
        ]);

        $params = [
            'valueInputOption' => "USER_ENTERED"
        ];
        
        $result = $service->spreadsheets_values->update($this->spreadsheetId, $range, $body, $params);
        printf("%d cells updated.", $result->getUpdatedCells());
    }
    public function moveStageReview($id){
        $response = $this->bx24->callMethod('crm.deal.update', [
            'ID' => $id,
            'FIELDS' => [
                "STAGE_ID" => "C8:FINAL_INVOICE"
            ]
        ])->result;

        return $response;
    }

}