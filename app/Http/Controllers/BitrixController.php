<?php

namespace App\Http\Controllers;

use App\Bitrix\Bitrix24Webhook;

class BitrixController extends Controller {

    public function __construct(){
        $webhook = "https://enerzee.bitrix24.com.br/rest/685/g4u5wqfctfg2dwtn/";
        $this->bx24 = new Bitrix24Webhook($webhook);
    }

    public function getFieldsList(){
        $params = [
            "IBLOCK_TYPE_ID" => "bitrix_processes",
            "IBLOCK_ID" => 40
        ];

        $fields = $this->bx24->callMethod("lists.field.get", $params)->result;

        return response()->json($fields);
    }

    public function getFieldsDeal($deal_id){
        $deal_id = $_GET['deal_id'];

        $deal = $this->bx24->callMethod("crm.deal.get", ["ID"=>$deal_id])->result;

        return $deal;
    }
    

}