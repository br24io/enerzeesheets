<?php

namespace App\Http\Controllers;

use App\Bitrix\Bitrix24Webhook;
use DateTime;
use DateTimeZone;

class TasksController extends Controller
{
    public function __construct()
    {
        $webhook = "https://enerzee.bitrix24.com.br/rest/685/g4u5wqfctfg2dwtn/";
        $this->bx24 = new Bitrix24Webhook($webhook);
    }

    public function processTasks(){
        
    }


    public function getList(){
        $data_criacao = "PROPERTY_291";
        $data_conclusao = "PROPERTY_293";
        $params = [
            "IBLOCK_TYPE_ID" => "bitrix_processes",
            "IBLOCK_ID" => 49,
        ];

        $lists = $this->bx24->callMethod("lists.element.get", $params)->result;

        return $lists;
    }

    public function countDate(){
        date_default_timezone_set('America/Sao_Paulo');
        $lists = $this->getList();
        foreach($lists as $list){
            $date_array = $list->PROPERTY_291;
            $list_aux = $list;
            foreach($date_array as $array => $date_created){
                $date_created = DateTime::createFromFormat("d/m/Y H:i:s", $date_created);
                echo "<p>";
                echo "Data de criação: ";
                echo $date_created->format('d-m-Y H:i:s');
                echo "<p>";
                echo "Data Atual: ";
                $data_atual = new DateTime();
                echo $data_atual->format('d-m-Y H:i:s');

                $diff = $data_atual->diff($date_created)->format("%d dias, %h horas and %i minutos");

                echo "<p>";
                echo $diff;

                //$update = $this->updateDiff($list_aux, $diff);

                exit;

            }
        }
    }

    public function updateDiff($list, $diff){
        $list->PROPERTY_297 = $diff;
        $params = [
            'IBLOCK_TYPE_ID'=> 'bitrix_processes',
            'IBLOCK_ID'=> 49,
            'ELEMENT_ID' => $list->ID,
            'FIELDS' => $list
        ];

        $response = $this->bx24->callMethod("lists.element.update", $params);

        return $response;
    }

}